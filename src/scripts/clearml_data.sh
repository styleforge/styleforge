#!/usr/bin/env bash

clearml-data create --project styleforge --name deepfashion2 \
 --parents 73617cd329fd4e51ac1aa4e7785532b8 --version 0.0.2

clearml-data add --files data --verbose

clearml-data close