#!/usr/bin/env bash
export PYTHONPATH='/nfs/home/lsbitneva/miniconda3/bin/python'
python -m torch.distributed.run --nproc_per_node 2 /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/train.py \
 --img 640 --data /mnt/tank/scratch/lsbitneva/styleforge/src/configs/deep_f_data.yaml \
 --cfg /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/models/yolov5n.yaml \
 --batch-size 512 --name testm --weights yolov5n.pt --epochs 50 \
 --device 4,5 --single-cls
 