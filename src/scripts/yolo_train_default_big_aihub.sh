#!/usr/bin/env bash
export PYTHONPATH='/nfs/home/lsbitneva/miniconda3/bin/python'
echo 'start copy'
cp -r /mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data_big /mnt/hot/aihub/lsbitneva/styleforge/data
echo 'finished copy'
python -m torch.distributed.run --nproc_per_node 2 /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/train.py \
 --img 640 --data /mnt/tank/scratch/lsbitneva/styleforge/src/configs/deep_f_databigaihub.yaml \
 --cfg /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/models/yolov5n.yaml \
 --batch-size 1024 --name Model_5n_aihub \
 --weights /mnt/tank/scratch/lsbitneva/yolov5n.pt --epochs 50 \
 --device 1,2 --single-cls

 rm -r /mnt/hot/aihub/lsbitneva/styleforge/data/yolo_data_big