import numpy as np


class Cutout(object):
    """Аугментация, которая вырезает кусок заданного размера из картинки
    статья: https://arxiv.org/pdf/1708.04552.pdf"""

    def __init__(self, size=8):
        self.size = size

    def __call__(self, tensor):
        _, h, w = tensor.shape
        y = np.random.randint(0, h)
        x = np.random.randint(0, w)
        tensor[:, y - self.size: y + self.size, x - self.size: x + self.size] = 0

        return tensor
