from typing import Union
from torch import nn
from torchvision import models, transforms
import timm

transformer_models = ["Vit", "Vit_16_dino"]
resnet_models = ["resnet50", "resnet18"]

def get_model(model_name: str, emb_size: int, with_preproc: bool=True) -> Union[list, nn.Module]:
    if model_name in transformer_models or model_name in resnet_models:
        resize_size = 256
        crop_size = 224

    train_transform = transforms.Compose(
        [
            transforms.Resize((resize_size, resize_size)),
            transforms.CenterCrop(crop_size),
            transforms.RandomHorizontalFlip(0.5),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    val_transform = transforms.Compose(
        [
            transforms.Resize((resize_size, resize_size)),
            transforms.CenterCrop(crop_size),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    
    if model_name == "Vit_16_dino":
        model = timm.create_model(
            "vit_base_patch16_224_dino", pretrained=True, num_classes=emb_size
        ).cuda()

    if model_name == "resnet18":
        model = models.resnet18(pretrained=True).cuda()

        for param in model.parameters():
            param.requires_grad = False

        model.fc = nn.Linear(512, emb_size).cuda()

    if model_name == "resnet50":
        model = models.resnet50(pretrained=True).cuda()

        for param in model.parameters():
            param.requires_grad = False

        model.fc = nn.Linear(2048, emb_size).cuda()

    if model_name == "Vit":
        model = models.vit_b_16(pretrained=True).cuda()

        for param in model.parameters():
            param.requires_grad = False

        model.heads[0] = nn.Linear(768, emb_size).cuda()
    if with_preproc:
        return [model, train_transform, val_transform]
    return model
    
    
