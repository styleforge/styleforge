import os
import random

import pandas as pd
import math
import seaborn as sns
from sklearn.manifold import TSNE
import torch
import torch.optim as optim
from pytorch_metric_learning import losses, testers
from pytorch_metric_learning.utils.accuracy_calculator import AccuracyCalculator
from clearml import Task
import matplotlib.pyplot as plt
from src.metric_learning.dataset import DeepFashionDataset
from src.metric_learning.cutout import Cutout
from src.metric_learning.models import get_model
import mlflow


seed = 123
device = torch.device("cuda")



# training cycle
def train(
    model,
    loss_func,
    device,
    train_loader,
    optimizer,
    loss_optimizer,
    epoch,
    iterations_in_epoch,
    logger,
):
    model.train()
    for batch_idx, (data, labels) in enumerate(train_loader):
        data, labels = data.to(device), labels.to(device)
        optimizer.zero_grad()
        loss_optimizer.zero_grad()
        embeddings = model(data)
        loss = loss_func(embeddings, labels)
        loss.backward()
        optimizer.step()
        loss_optimizer.step()

        if batch_idx % 50 == 0:
            logger.report_scalar(
                title="Loss",
                series="Train loss",
                iteration=iterations_in_epoch * (epoch) + batch_idx,
                value=loss.item(),
            )
            print("Epoch {} Iteration {}: Loss = {}".format(epoch, batch_idx, loss))


# get all embeddings from dataset
def get_all_embeddings(dataset, model, batch_size):
    tester = testers.BaseTester(dataloader_num_workers=8, batch_size=batch_size)
    return tester.get_all_embeddings(dataset, model)


# compute accuracy using AccuracyCalculator from pytorch-metric-learning
def test(
    query_dataset,
    retrieval_dataset,
    model,
    accuracy_calculator,
    epoch,
    iterations_in_epoch,
    logger,
    batch_size
):
    query_embeddings, query_labels = get_all_embeddings(query_dataset, model, batch_size)
    query_labels = query_labels.squeeze(1)

    print("Computing accuracy")
    # it is important if query=retrieval!!! ref_includes_query=True
    accuracies = accuracy_calculator.get_accuracy(
        query_embeddings,
        query_labels,
        query_embeddings,
        query_labels,
        ref_includes_query=True,
    )
    logger.report_scalar(
        title="Metrics",
        series="Precision@1",
        iteration=iterations_in_epoch * (epoch + 1),
        value=accuracies["precision_at_1"],
    )
    mlflow.log_metric(
        key="Precision_1",
        value=accuracies["precision_at_1"],
        step=iterations_in_epoch * (epoch + 1)
    )
    logger.report_scalar(
        title="Metrics",
        series="mAP",
        iteration=iterations_in_epoch * (epoch + 1),
        value=accuracies["mean_average_precision"],
    )
    mlflow.log_metric(
        key="mAP",
        value=accuracies["mean_average_precision"],
        step=iterations_in_epoch * (epoch + 1)
    )
    print("Creating visualisation")
    if epoch % 5 == 0:
        comb_emb = query_embeddings.cpu().numpy()
        comb_lab = query_labels.cpu().numpy()

        comb_tsne = TSNE(metric="cosine", random_state=seed).fit_transform(comb_emb)

        sns.scatterplot(
            x=comb_tsne[:, 0],
            y=comb_tsne[:, 1],
            hue=comb_lab,
            palette="Paired",
        )
        plt.title("Query & Retrieval Embeddings tSNE")
        plt.legend([], [])
        logger.report_matplotlib_figure(
            "Class Embeddings tSNE",
            "series",
            plt.gcf(),
            iteration=iterations_in_epoch * (epoch + 1),
        )
        plt.clf()

    print("Test set accuracy (Precision@1) = {}".format(accuracies["precision_at_1"]))
    print("Test set map = {}".format(accuracies["mean_average_precision"]))


def run(opt):
    epochs = opt["epochs"]
    batch_size = opt["batch_size"]
    base_lr = opt["base_lr"]
    emb_size = opt["emb_size"]
    cutout = opt["cutout"]
    model_name = opt["model"]
    dataset_type = opt["dataset_type"]
    optimizer_name = opt['optimizer']
    df_train_path = opt['df_train']
    df_val_path = opt['df_val']
    # take part for mini dataset
    split_ = 0.1
    aug = "_cutout" if cutout else ""
    task_name = f"{model_name}_{base_lr}_{dataset_type}{aug}"
    # Logging
    task = Task.init(project_name="styleforge/metric_learning", task_name=task_name)
    mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))
    mlflow.set_experiment("styleforge/metric_learning")
    mlflow.start_run(run_name=task_name.replace('.', ''))
    configuration_dict = {
        "number_of_epochs": epochs,
        "seed": seed,
        "batch_size": batch_size,
        "dropout": 0.25,
        "base_lr": base_lr,
    }
    configuration_dict = task.connect(
        configuration_dict
    )  # enabling configuration override by clearml
    mlflow.log_params(configuration_dict)

    model_storage = f"models/{task.task_id}"
    if not os.path.exists(model_storage):
        os.makedirs(model_storage)  # create model storage
    logger = task.get_logger()

    # Data
    print(df_train_path)
    df_train = pd.read_csv(
        df_train_path
    )
    df_val = pd.read_csv(
        df_val_path
    )
    num_classes_train = len(
        df_train.label.unique()
    )  # reassign class numbers so it doesn't go over num_classes_train
    df_train["label"] = df_train.groupby(["label"]).ngroup()
    model, train_transform, val_transform = get_model(model_name, emb_size, with_preproc=True)
    
    if cutout:
        train_transform.transforms.append(Cutout(size=14))

    train_dataset = val_dataset = None
    if dataset_type == "mini":
        """
        #create mini datasets, we don't divide classes between train and val,
        #take 10% train and 10% val data for mini train and val if split_ = 0.1
        """
        # train
        cls_ids_train = df_train.label.unique().tolist()
        train_mini_cls_ids = random.sample(cls_ids_train, int(len(cls_ids_train) * split_))

        # val
        cls_ids_val = df_val.label.unique().tolist()
        val_mini_cls_ids = random.sample(cls_ids_val, int(len(cls_ids_val) * split_))

        df_train_mini = df_train.loc[df_train["label"].isin(train_mini_cls_ids)]
        df_val_mini = df_val.loc[df_val["label"].isin(val_mini_cls_ids)]

        train_dataset = DeepFashionDataset(df_train_mini, transform=train_transform)
        val_dataset = DeepFashionDataset(df_val_mini, transform=val_transform)

    if dataset_type == "big":
        train_dataset = DeepFashionDataset(df_train, transform=train_transform)
        val_dataset = DeepFashionDataset(df_val, transform=val_transform)

    iterations_in_epoch = math.ceil(len(train_dataset) / batch_size)

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=8, drop_last=True
    )

    if optimizer_name == 'adam':
        optimizer = optim.Adam(model.parameters(), lr=base_lr)
    elif optimizer_name == "sgd":
        optimizer = optim.SGD(model.parameters(), lr=base_lr, momentum=0.9)
    loss_func = losses.SubCenterArcFaceLoss(
        num_classes=num_classes_train, embedding_size=512
    ).to(device)
    loss_optimizer = torch.optim.Adam(loss_func.parameters(), lr=1e-4)
    accuracy_calculator = AccuracyCalculator(
        include=(
            "precision_at_1",
            "mean_average_precision",
        ),
        k="max_bin_count",
    )

    # Start training
    for epoch in range(0, epochs):
        train(
            model,
            loss_func,
            device,
            train_loader,
            optimizer,
            loss_optimizer,
            epoch,
            iterations_in_epoch,
            logger,
        )
        test(
            val_dataset,
            val_dataset,
            model,
            accuracy_calculator,
            epoch,
            iterations_in_epoch,
            logger,
            batch_size
        )
        if (epoch + 1) % 5 == 0:
            torch.save(model, f"{model_storage}/model_ckpt_epoch_{epoch + 1}.pth")
            mlflow.log_artifact(f"{model_storage}/model_ckpt_epoch_{epoch + 1}.pth")

# srun --cpus-per-task=20 -p aihub --gres=gpu --mem=50G --time=50:00:00 --pty bash
#
# #gpu:4

"""
sbatch --cpus-per-task=10 -p aihub --gres=gpu --mem=50G  \
 --time=76:00:00 /mnt/tank/scratch/lsbitneva/styleforge/src/scripts/metric_l_run.sh
"""
