
import json
import os

import cv2

"""
Parse all .json file in a folder to Yolo format ,and save it to the folder /labels.
Yolo format: (in .txt)
    <object-class> <x> <y> <width> <height>
<object-class> : integer number of object, range: [0, num_classes-1]
<x> = <absolute_x> / <image_width>, range: [0,1]
<y> = <absolute_y> / <image_height>, range: [0,1]
<width> = <absolute_width> / <image_width>, range: [0,1]
<height> = <absolute_height> / <image_height>, range: [0,1]
"""

anno_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/raw/train/annos"

img_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/raw/train/image"

lbl_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data_big/train/labels"

print(len(os.listdir(anno_dir_name)))
print(len(os.listdir(img_dir_name)))
print(len(os.listdir(lbl_dir_name)))

for filename in os.listdir(anno_dir_name):  # loop throught the entire folder
    if filename.lower().endswith(".json") is False:
        continue

    file_path = os.path.join(anno_dir_name, filename)
    #    print(file_path)
    with open(file_path, "r") as json_file:
        data = json.load(json_file)
        num_object = len(data) - 2

        with open(
            os.path.join(lbl_dir_name, filename.split(".")[0] + ".txt"), "w"
        ) as yolo_file:
            img = cv2.imread(
                os.path.join(img_dir_name, filename.split(".")[0] + ".jpg")
            )  # load input image
            img_width = img.shape[1]
            img_height = img.shape[0]
            for i in range(1, num_object + 1):  # for each object
                class_id = data["item{}".format(i)]["category_id"] - 1
                x1 = data["item{}".format(i)]["bounding_box"][0]
                y1 = data["item{}".format(i)]["bounding_box"][1]
                x2 = data["item{}".format(i)]["bounding_box"][2]
                y2 = data["item{}".format(i)]["bounding_box"][3]

                yolo_x = (x1 + x2) / img_width / 2  # relative center x
                yolo_y = (y1 + y2) / img_height / 2  # relative center y
                yolo_width = (x2 - x1) / img_width
                yolo_height = (y2 - y1) / img_height

                # output YOLO format
                yolo_file.write(
                    "{} {:.6f} {:.6f} {:.6f} {:.6f}\n".format(
                        class_id, yolo_x, yolo_y, yolo_width, yolo_height
                    )
                )

print(len(os.listdir(anno_dir_name)))
print(len(os.listdir(img_dir_name)))
print(len(os.listdir(lbl_dir_name)))
