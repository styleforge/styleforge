
import json
import os
import random

import cv2
from matplotlib.pyplot import figure

anno_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/raw/validation/annos"

img_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/raw/validation/image"

lbl_dir_name = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data/train/labels"

annos = os.listdir(anno_dir_name)

print(len(annos))

classes = {
    0: "short sleeve top",
    1: "long sleeve top",
    2: "short sleeve outwear",
    3: "long sleeve outwear",
    4: "vest",
    5: "sling",
    6: "shorts",
    7: "trousers",
    8: "skirt",
    9: "short sleeve dress",
    10: "long sleeve dress",
    11: "vest dress",
    12: "sling dress",
}

# visualization bboxes
annos_random = random.sample(annos, 1)


figure(figsize=(18, 16), dpi=80)
for filename in annos_random:  # loop throught the entire folder
    if filename.lower().endswith(".json") is False:
        continue

    file_path = os.path.join(anno_dir_name, filename)
    #    print(file_path)
    with open(file_path, "r") as json_file:
        data = json.load(json_file)
        num_object = len(data) - 2

        with open(
            os.path.join(lbl_dir_name, filename.split(".")[0] + ".txt"), "w"
        ) as yolo_file:
            img = cv2.imread(
                os.path.join(img_dir_name, filename.split(".")[0] + ".jpg")
            )  # load input image
            img_width = img.shape[1]
            img_height = img.shape[0]
            for i in range(1, num_object + 1):  # for each object
                class_id = data["item{}".format(i)]["category_id"] - 1
                x1 = data["item{}".format(i)]["bounding_box"][0]
                y1 = data["item{}".format(i)]["bounding_box"][1]
                x2 = data["item{}".format(i)]["bounding_box"][2]
                y2 = data["item{}".format(i)]["bounding_box"][3]

                yolo_x = (x1 + x2) / img_width / 2  # relative center x
                yolo_y = (y1 + y2) / img_height / 2  # relative center y
                yolo_width = (x2 - x1) / img_width
                yolo_height = (y2 - y1) / img_height

                # output YOLO format
                cv2.rectangle(
                    img,
                    (int(x1), int(y1)),
                    (int(x2), int(y2)),
                    color=(0, 255, 0),
                    thickness=2,
                )
                ((label_width, label_height), _) = cv2.getTextSize(
                    classes[class_id],
                    fontFace=cv2.FONT_HERSHEY_PLAIN,
                    fontScale=1.75,
                    thickness=2,
                )
                cv2.rectangle(
                    img,
                    (int(x1), int(y1)),
                    (
                        int(x1 + label_width + label_width * 0.05),
                        int(y1 + label_height + label_height * 0.25),
                    ),
                    color=(0, 255, 0),
                    thickness=cv2.FILLED,
                )
                cv2.putText(
                    img,
                    classes[class_id],
                    org=(
                        int(x1),
                        int(y1 + label_height + label_height * 0.25),
                    ),  # bottom left
                    fontFace=cv2.FONT_HERSHEY_PLAIN,
                    fontScale=1.75,
                    color=(255, 255, 255),
                    thickness=2,
                )

                cv2.imwrite(
                    "/mnt/tank/scratch/lsbitneva/styleforge/data/raw/viz_bb.jpg", img
                )
