import os
import random
import shutil

from tqdm import tqdm

# move random 20% from train to val
src_x_train_dir = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data/train/images"
img_names = os.listdir(src_x_train_dir)

src_y_train_dir = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data/train/labels"
label_names = os.listdir(src_y_train_dir)

val_dir_x = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data/val/images"
val_dir_y = "/mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data/val/labels"

img_names = [x[:-4] for x in os.listdir(src_x_train_dir)]
label_names = [x[:-4] for x in os.listdir(src_y_train_dir)]

print(len(img_names))
print(len(label_names))

val_names = random.sample(img_names, int(len(img_names) * 0.2))

for i, name in tqdm(
    enumerate(val_names), total=len(val_names), desc="extracting validation part"
):
    shutil.move(
        os.path.join(src_x_train_dir, name + ".jpg"),
        os.path.join(val_dir_x, name + ".jpg"),
    )
    shutil.move(
        os.path.join(src_y_train_dir, name + ".txt"),
        os.path.join(val_dir_y, name + ".txt"),
    )

print(len(os.listdir(val_dir_x)))
print(len(os.listdir(val_dir_y)))
print(len(os.listdir(src_x_train_dir)))
print(len(os.listdir(src_y_train_dir)))
