import click
from src.metric_learning.train import run
import os
from dotenv import load_dotenv

@click.group()
def cli():
    click.echo("Starting cli")

@cli.command()
@click.option('--epochs', default=10, help='number of epochs')
@click.option('--batch_size', default=8, help='size of a batch for training')
@click.option('--base_lr', default=0.01, help='starting learing rate')
@click.option('--emb_size', default=512, help='embedding size')
@click.option('--cutout', is_flag=True, help='apply CutOut augmentation')
@click.option('--resnet50', 'model', flag_value='resnet50', default=True, help='use resnet50 model')
@click.option('--resnet18', 'model', flag_value='resnet18', help='use resnet18 model')
@click.option('--vit', 'model', flag_value='Vit', help='use ViT model')
@click.option('--vit_16_dino', 'model', flag_value='Vit_16_dino', help='use ViT16_dino model')
@click.option('--dataset_type', type=click.Choice(['big', 'mini'], case_sensitive=False), help='which dataset to use')
@click.option('--optimizer', type=click.Choice(['adam', 'sgd'], case_sensitive=False), default='adam', help='which dataset to use')
@click.option('--df_train', default=os.path.abspath('preproc_csv/total_annos.csv'), help='path to train dataframe')
@click.option('--df_val', default=os.path.abspath('preproc_csv/total_annos.csv'), help='path to val dataframe')
def metric_learning(**kwargs):
    load_dotenv('.env')
    run(kwargs)


if __name__ == "__main__":
    cli()

# python start_training.py metric-learning --dataset_type=big