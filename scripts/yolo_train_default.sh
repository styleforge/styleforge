#!/usr/bin/env bash
export PYTHONPATH='/nfs/home/lsbitneva/miniconda3/bin/python'
python /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/train.py \
 --img 640 --data /mnt/tank/scratch/lsbitneva/styleforge/src/configs/deep_f_data.yaml \
 --cfg /mnt/tank/scratch/lsbitneva/styleforge/src/yolov5/models/yolov5n.yaml \
 --batch-size 128 --name Model_5n --weights yolov5n.pt --epochs 50 \
 --device 0,1,2,3