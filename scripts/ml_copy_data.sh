#!/usr/bin/env bash
export PYTHONPATH='/nfs/home/lsbitneva/miniconda3/bin/python'
echo 'start copy'
mkdir -p /mnt/hot/aihub/lsbitneva/styleforge/data/train/images
cp -r /mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data_big/train/images /mnt/hot/aihub/lsbitneva/styleforge/data/train/images
echo 'finished copy train'
mkdir -p /mnt/hot/aihub/lsbitneva/styleforge/data/val/images
cp -r /mnt/tank/scratch/lsbitneva/styleforge/data/yolo_data_big/val/images /mnt/hot/aihub/lsbitneva/styleforge/data/val/images
echo 'finished copy val'

#conda install -c conda-forge pytorch-metric-learning