#!/usr/bin/env bash

#cp /mnt/hot/aihub/lsbitneva/styleforge/data


#sbatch --cpus-per-task=10 -p aihub --gres=gpu:2 --mem=50G \
# --time=50:00:00 /mnt/tank/scratch/lsbitneva/styleforge/src/scripts/yolo_train_default_big_aihub.sh

#echo 'start sbatch'

#sbatch --cpus-per-task=10 -p aihub --gres=gpu --mem=50G  \
# --time=76:00:00 /mnt/hot/aihub/lsbitneva/styleforge/data/met_learn_aihub1.sh

sbatch --cpus-per-task=10 -p aihub --gres=gpu --mem=50G  \
 --time=76:00:00 /mnt/tank/scratch/lsbitneva/styleforge/src/metric_learning/train.py